import {ICard} from './ICards';

export interface IListCards {
  listId: number;
  flashCards: ICard[];
}
