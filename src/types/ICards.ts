export interface ICard {
  id: number;
  english: string;
  vietnamese: string;
}

export interface IResCard {
  success: boolean;
  message: string;
  flashCards: ICard[];
}
export interface IResCreateCard {
  success: boolean;
  message: string;
  flashCard: ICard;
}

export interface IResDeleteCard {
  success: boolean;
  message: string;
  flashCard: null;
}
