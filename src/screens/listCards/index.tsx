import {RouteProp, useRoute} from '@react-navigation/core';
import React from 'react';
import {Alert, StyleSheet, Text} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import {TouchableOpacity, View} from 'react-native-ui-lib';
import {useDispatch, useSelector} from 'react-redux';
import {RootStackParamList} from '../../nav/RootStack';
import {RootState} from '../../reduxs/store';
import {ICard, IResCard} from '../../types/ICards';
import ItemCard from './components/ItemCard';
import URL from '../../config/Api';
import {onUpdateList} from '../../reduxs/listCardsSlice';
import AddCardModal from './components/AddCardModal';
import {IListCards} from '../../types/IListCards';

const ListCards = () => {
  const route = useRoute<RouteProp<RootStackParamList, 'ListCards'>>();
  const listId = route.params.listId;
  const dispatch = useDispatch();
  const listInfo = useSelector<RootState, IListCards>(state => state.listCards);
  const listCards = listInfo.flashCards;
  const [loading, setLoading] = React.useState(false);
  const token = useSelector<RootState, string>(state => state.auth.token);
  const uid = useSelector<RootState, number>(state => state.auth.user.id);

  React.useEffect(() => {
    if (!token) {
      return;
    }
    setLoading(true);
    console.log(token);
    fetch(
      URL.FlashCards({
        uid: uid,
        listId: listId,
      }),
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      },
    )
      .then(response => response.json())
      .then((json: IResCard) => {
        // console.log(json);
        const success = json.success;
        //login fail
        if (!success) {
          console.log(success);
          Alert.alert('Thông báo', json.message);
          setLoading(false);
          return;
        }
        // LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
        // setListCards(json.flashCards);
        dispatch(
          onUpdateList({
            listId: listId,
            flashCards: json.flashCards,
          }),
        );
        setLoading(false);
        console.log(json.flashCards);
        return json;
      })
      .catch(e => console.log(e));
  }, [listId]);

  const renderItem = React.useCallback(
    ({item, index}: {item: ICard; index: number}) => {
      return (
        <View>
          {!loading ? (
            <ItemCard item={item} index={index} />
          ) : (
            <View>
              <Text>LOADING</Text>
            </View>
          )}
        </View>
      );
    },
    [loading],
  );

  return (
    <View flex>
      <AddCardModal />
      <Text>{`List Card ${listId}`}</Text>
      <FlatList
        data={listCards}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        extraData={listCards}
      />
    </View>
  );
};

export default ListCards;

const styles = StyleSheet.create({});
