import React from 'react';
import {Alert, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {onUpdateItem} from '../../../reduxs/listCardsSlice';
import {RootState} from '../../../reduxs/store';
import {ICard, IResCreateCard} from '../../../types/ICards';
import URL from '../../../config/Api';

const BtnUpdate = ({card, dismiss}: {card: ICard; dismiss: () => void}) => {
  const token = useSelector<RootState, string>(state => state.auth.token);
  const [loading, setLoading] = React.useState<boolean>(false);
  const dispatch = useDispatch();

  const onPressSave = React.useCallback(() => {
    console.log(card);
    if (!token) return;
    fetch(URL.FlashCard, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        fcId: card.id,
        english: card.english,
        vietnamese: card.vietnamese,
      }),
    })
      .then(response => response.json())
      .then((json: IResCreateCard) => {
        const success = json.success;
        //add fail
        if (!success) {
          Alert.alert('Thông báo', json.message);
          setLoading(false);
          return;
        }
        //add success
        dispatch(onUpdateItem(json.flashCard));
        setLoading(false);
        // saveAuthAsync(json); //save token to async storage
        dismiss();
        return json;
      })
      .catch(error => {
        console.error(error);
      });
  }, [card]);
  return (
    <TouchableOpacity
      style={[styles.button, styles.buttonClose]}
      onPress={onPressSave}>
      <Text style={styles.textStyle}>Update</Text>
    </TouchableOpacity>
  );
};

export default BtnUpdate;

const styles = StyleSheet.create({
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
