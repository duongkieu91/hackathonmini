import React from 'react';
import {Alert, StyleSheet, TextInput, TouchableOpacity} from 'react-native';
import {Modal, Text, View} from 'react-native-ui-lib';
import {ICard} from '../../../types/ICards';
import BtnSave from './BtnSave';
import BtnUpdate from './BtnUpdate';

const EditCardModal = ({initCard}: {initCard: ICard}) => {
  const [cardInfo, setCardInfo] = React.useState(initCard);
  const [modalVisible, setModalVisible] = React.useState<boolean>(false);

  return (
    <View>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View>
              <TextInput
                defaultValue={initCard.english}
                placeholder={'FRONT'}
                style={{color: '#000'}}
                onChangeText={(english: string) =>
                  setCardInfo(prev => {
                    return {
                      ...prev,
                      english,
                    };
                  })
                }
              />
              <View
                style={{borderBottomColor: '#f0f0f0', borderBottomWidth: 1}}
              />
              <TextInput
                defaultValue={initCard.vietnamese}
                placeholder={'BACK'}
                style={{color: '#000'}}
                onChangeText={(vietnamese: string) =>
                  setCardInfo(prev => {
                    return {
                      ...prev,
                      vietnamese,
                    };
                  })
                }
              />
            </View>
            <View row>
              <BtnUpdate
                card={cardInfo}
                dismiss={() => {
                  setModalVisible(false);
                }}
              />
              <TouchableOpacity
                style={[styles.button, styles.buttonClose]}
                onPress={() => {
                  setModalVisible(!modalVisible);
                }}>
                <Text style={styles.textStyle}>Dismiss</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      <TouchableOpacity
        onPress={() => {
          setModalVisible(true);
        }}>
        <Text>V</Text>
      </TouchableOpacity>
    </View>
  );
};

export default EditCardModal;

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
});
