import {NavigationProp, useNavigation} from '@react-navigation/native';
import React from 'react';
import {Alert, StyleSheet} from 'react-native';
import {TextInput} from 'react-native';
import {TouchableOpacity, View, Text} from 'react-native-ui-lib';
import {RootStackParamList} from '../../../nav/RootStack';
import {ICard, IResDeleteCard} from '../../../types/ICards';
import URL from '../../../config/Api';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../../../reduxs/store';
import {onDeleteItem} from '../../../reduxs/listCardsSlice';
import EditCardModal from './EditCardModal';

const ItemCard = ({item, index}: {item: ICard; index: number}) => {
  const {navigate} = useNavigation<NavigationProp<RootStackParamList>>();
  const token = useSelector<RootState, string>(state => state.auth.token);
  const userId = useSelector<RootState, number>(state => state.auth.user.id);
  const listId = useSelector<RootState, number>(
    state => state.listCards.listId,
  );
  const dispatch = useDispatch();
  const onPressDelete = React.useCallback(() => {
    fetch(URL.FlashCard, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        fcId: item.id,
      }),
    })
      .then(response => response.json())
      .then((json: IResDeleteCard) => {
        const success = json.success;
        //login fail
        if (!success) {
          Alert.alert('Thông báo', json.message);
          // setLoading(false);
          return;
        }
        //login success
        dispatch(onDeleteItem(item));
        // setLoading(false);

        return json;
      })
      .catch(error => {
        console.error(error);
      });
  }, []);

  return (
    <View row>
      <TouchableOpacity
        flex
        onPress={() => {
          navigate('Study', {index});
        }}>
        <View
          style={{
            marginHorizontal: 12,
            elevation: 3,
            backgroundColor: '#fff',
            marginBottom: 6,
          }}>
          <TextInput
            value={item.english}
            editable={false}
            style={{color: '#000'}}
          />
          <View style={{borderBottomColor: '#f0f0f0', borderBottomWidth: 1}} />
          <TextInput
            value={item.vietnamese}
            editable={false}
            style={{color: '#000'}}
          />
        </View>
      </TouchableOpacity>
      <View>
        <TouchableOpacity
          backgroundColor={'red'}
          flex
          width-100
          onPress={() => {
            console.log('xoa flash card id= ', item.id);
            onPressDelete();
          }}>
          <Text>X</Text>
        </TouchableOpacity>
        <TouchableOpacity
          backgroundColor={'green'}
          flex
          width-100
          onPress={() => {
            console.log('edit flash card id= ', item.id);
            // onPressDelete();
          }}>
          <EditCardModal initCard={item} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ItemCard;

const styles = StyleSheet.create({});
