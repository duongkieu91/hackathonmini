import React from 'react';
import {Alert, StyleSheet, TextInput, TouchableOpacity} from 'react-native';
import {Modal, Text, View} from 'react-native-ui-lib';
import BtnSave from './BtnSave';

const AddCardModal = () => {
  const [cardInfo, setCardInfo] = React.useState({
    userId: -1,
    listId: -1,
    english: '',
    vietnamese: '',
  });

  const [modalVisible, setModalVisible] = React.useState<boolean>(false);

  return (
    <View>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View>
              <TextInput
                // value={front}
                placeholder={'FRONT'}
                style={{color: '#000'}}
                onChangeText={(english: string) =>
                  setCardInfo(prev => {
                    return {
                      ...prev,
                      english,
                    };
                  })
                }
              />
              <View
                style={{borderBottomColor: '#f0f0f0', borderBottomWidth: 1}}
              />
              <TextInput
                // value={back}
                placeholder={'BACK'}
                style={{color: '#000'}}
                onChangeText={(vietnamese: string) =>
                  setCardInfo(prev => {
                    return {
                      ...prev,
                      vietnamese,
                    };
                  })
                }
              />
            </View>
            <View row>
              <BtnSave cardInfo={cardInfo} />
              <TouchableOpacity
                style={[styles.button, styles.buttonClose]}
                onPress={() => {
                  setModalVisible(!modalVisible);
                }}>
                <Text style={styles.textStyle}>Dismiss</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      <TouchableOpacity
        onPress={() => {
          setModalVisible(true);
        }}>
        <Text>Add card</Text>
      </TouchableOpacity>
    </View>
  );
};

export default AddCardModal;

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
});
