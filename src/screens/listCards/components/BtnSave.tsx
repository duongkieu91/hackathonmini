import React from 'react';
import {Alert, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {onAddItem} from '../../../reduxs/listCardsSlice';
import {RootState} from '../../../reduxs/store';
import {IResCreateCard} from '../../../types/ICards';
import URL from '../../../config/Api';

interface Props {
  cardInfo: {
    userId: number;
    listId: number;
    english: string;
    vietnamese: string;
  };
}

const BtnSave = ({cardInfo}: Props) => {
  const userId = useSelector<RootState, number>(state => state.auth.user.id);
  const token = useSelector<RootState, string>(state => state.auth.token);
  const [loading, setLoading] = React.useState<boolean>(false);
  const dispatch = useDispatch();

  const listId = useSelector<RootState, number>(
    state => state.listCards.listId,
  );
  const onPressSave = React.useCallback(() => {
    console.log(cardInfo);
    if (!token) return;
    fetch(URL.FlashCard, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        userId: userId,
        listId: listId,
        english: cardInfo.english,
        vietnamese: cardInfo.vietnamese,
      }),
    })
      .then(response => response.json())
      .then((json: IResCreateCard) => {
        const success = json.success;
        //add fail
        if (!success) {
          Alert.alert('Thông báo', json.message);
          setLoading(false);
          return;
        }
        //add success
        dispatch(onAddItem(json.flashCard));
        setLoading(false);
        // saveAuthAsync(json); //save token to async storage
        return json;
      })
      .catch(error => {
        console.error(error);
      });

  }, [cardInfo]);
  return (
    <TouchableOpacity
      style={[styles.button, styles.buttonClose]}
      onPress={onPressSave}>
      <Text style={styles.textStyle}>Save</Text>
    </TouchableOpacity>
  );
};

export default BtnSave;

const styles = StyleSheet.create({
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
