import React from 'react';
import {StyleSheet} from 'react-native';
import {TouchableOpacity, Text, View} from 'react-native-ui-lib';

const ItemStudyCard = ({front, back}: {front: string; back: string}) => {
  const [isFront, setIsFront] = React.useState<boolean>(true);
  // React.useEffect(() => {
  //   setIsFront(true);
  // }, []);
  return (
    <TouchableOpacity
      style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}
      onPress={() => {
        setIsFront(prev => !prev);
      }}>
      {isFront ? (
        <View backgroundColor={'red'}>
          <Text h27>{`${front}`}</Text>
        </View>
      ) : (
        <Text h27>{`${back}`}</Text>
      )}
    </TouchableOpacity>
  );
};

export default ItemStudyCard;

const styles = StyleSheet.create({});
