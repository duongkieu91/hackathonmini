import {useRoute, RouteProp} from '@react-navigation/core';
import React from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import {Carousel, Colors, Text, View} from 'react-native-ui-lib';
import {useSelector} from 'react-redux';
import {RootStackParamList} from '../../nav/RootStack';
import {RootState} from '../../reduxs/store';
import {ICard} from '../../types/ICards';
import {IListCards} from '../../types/IListCards';
import ItemStudyCard from './components/ItemStudyCard';

const widthScreen = Dimensions.get('window').width;

const widthCarousel = widthScreen - 32;
const heightCarousel = (widthCarousel / 344) * 242;

const ItemBanner = ({item}: {item: ICard}) => {
  return (
    <View flex centerV backgroundColor={'green'}>
      <ItemStudyCard front={item.english} back={item.vietnamese} />
    </View>
  );
};

const Study = () => {
  const route = useRoute<RouteProp<RootStackParamList, 'Study'>>();
  const listInfo = useSelector<RootState, IListCards>(state => state.listCards);
  const renderItem = React.useCallback((item, index) => {
    return <ItemBanner key={index} item={item} />;
  }, []);
  return (
    <View>
      <Text>{`card id: ${route.params.index}`}</Text>

      <View style={styles.container}>
        <Carousel
          autoplay={false}
          pageWidth={widthCarousel}
          containerStyle={{height: heightCarousel}}
          loop
          initialPage={route.params.index}
          pageControlProps={{
            size: 10,
            containerStyle: styles.loopCarousel,
            color: Colors.primary,
            inactiveColor: Colors.white,
            // limitShownPages: true,
          }}
          // allowAccessibleLayout
          pageControlPosition={Carousel.pageControlPositions.OVER}>
          {listInfo.flashCards.map(renderItem)}
        </Carousel>
      </View>
    </View>
  );
};

export default Study;

const styles = StyleSheet.create({
  loopCarousel: {
    position: 'absolute',
    bottom: 15,
    left: 10,
  },
  container: {
    borderRadius: 12,
    overflow: 'hidden',
    marginTop: heightCarousel / 2 - 32,
    width: widthCarousel,
    height: heightCarousel,
    marginHorizontal: 16,
    marginBottom: 16,
  },
});
