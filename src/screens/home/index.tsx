import {NavigationProp, useNavigation} from '@react-navigation/core';
import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-ui-lib';
import {RootStackParamList} from '../../nav/RootStack';

const Home = () => {
  const {navigate} = useNavigation<NavigationProp<RootStackParamList>>();
  return (
    <View>
      <Text>HOME PAGE</Text>
      <Text>Add Set of Flash Card</Text>
      <TouchableOpacity
        onPress={() => {
          navigate('ListCards', {listId: 1});
        }}>
        <Text>LIST 1</Text>
      </TouchableOpacity>
      <TouchableOpacity>
        <Text>LIST 2</Text>
      </TouchableOpacity>
      <TouchableOpacity>
        <Text>LIST 3</Text>
      </TouchableOpacity>
      <TouchableOpacity>
        <Text>LIST 4</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({});
