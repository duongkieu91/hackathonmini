const MainURL = 'http://rn17-flash-card.herokuapp.com/';

const URL = {
  Login: MainURL + `api/login`,
  Register: MainURL + `api/register`,
  ValidateToken: MainURL + `validate`,
  CardList: MainURL + `api/list`,
  FlashCard: MainURL + `api/flashCard`, //POST PUT DELETE
  FlashCards: ({uid, listId}: {uid: number; listId: number}) =>
    MainURL + `api/flashCards?userId=${uid}&listId=${listId}`, // get flash cards
};

export default URL;
