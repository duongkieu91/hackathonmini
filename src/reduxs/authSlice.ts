import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import AsyncStorage from '@react-native-async-storage/async-storage';

export enum EGender {
  male = 1,
  femail = 2,
}

export interface IUser {
  created_at: string;
  deleted_at: null;
  email: string;
  email_verified_at: null;
  id: number;
  name: string;
  role_id: number;
  theme: string;
  updated_at: string;
}

export enum EStatusAuth {
  check = 1,
  unauth = 2,
  auth = 3,
}
export interface IAuth {
  user: IUser;
  message: string;
  success: boolean;
  token: string;
  statusAuth: EStatusAuth;
}

const initValue: IAuth = {
  user: {
    created_at: '',
    deleted_at: null,
    email: '',
    email_verified_at: null,
    id: -1,
    name: '',
    role_id: 3,
    theme: '',
    updated_at: '',
  },
  message: '',
  success: false,
  token: '',
  statusAuth: EStatusAuth.check,
};

export const authSlice = createSlice({
  name: 'auth',
  initialState: initValue,
  reducers: {
    onLogin: (state, action: PayloadAction<IAuth>) => {
      state.user = action.payload.user;
      state.message = action.payload.message;
      state.success = action.payload.success;
      state.token = action.payload.token;
      state.statusAuth = EStatusAuth.auth;
    },
    updateStatusAuth: (
      state,
      action: PayloadAction<{statusAuth: EStatusAuth}>,
    ) => {
      state.statusAuth = action.payload.statusAuth;
    },
  },
});

export const {onLogin, updateStatusAuth} = authSlice.actions;

export const saveAuthAsync = (auth: IAuth) => {
  try {
    AsyncStorage.setItem('RN17Auth', JSON.stringify(auth));
  } catch (e) {
    // saving error
  }
};

export const getAuthAsync = async () => {
  try {
    const auth = await AsyncStorage.getItem('RN17Auth');
    if (auth) {
      return JSON.parse(auth);
    }
    return null;
  } catch (e) {
    // error reading value
    return null;
  }
};

export default authSlice.reducer;
