import {createSlice, PayloadAction} from '@reduxjs/toolkit';

import {ICard} from '../types/ICards';
import {IListCards} from '../types/IListCards';

const initValue: IListCards = {
  listId: -1,
  flashCards: [
    {
      id: -1,
      english: '',
      vietnamese: '',
    },
  ],
};

export const listCardsSlice = createSlice({
  name: 'listCards',
  initialState: initValue,
  reducers: {
    onUpdateList: (state, action: PayloadAction<IListCards>) => {
      state.flashCards = action.payload.flashCards;
      state.listId = action.payload.listId;
    },
    onAddItem: (state, action: PayloadAction<ICard>) => {
      state.flashCards = [action.payload, ...state.flashCards];
    },
    onDeleteItem: (state, action: PayloadAction<ICard>) => {
      state.flashCards = state.flashCards.filter(item => {
        return item.id != action.payload.id;
      });
    },
    onUpdateItem: (state, action: PayloadAction<ICard>) => {
      state.flashCards = state.flashCards.map(item => {
        return item.id !== action.payload.id ? item : action.payload;
      });
    },
  },
});

// Action creators are generated for each case reducer function
export const {onUpdateList, onAddItem, onDeleteItem, onUpdateItem} =
  listCardsSlice.actions;

export default listCardsSlice.reducer;
