export const DATA = [
  {
    id: 1,
    english: 'hello 1',
    vietnamese: 'xin chao 1',
  },
  {
    id: 2,
    english:
      'good bye 2good bye 2good bye 2good bye 2good bye 2good bye 2good bye 2good bye 2good bye 2good bye 2good bye 2',
    vietnamese:
      'tam biet 2tam biet 2tam biet 2tam biet 2tam biet 2tam biet 2tam biet 2tam biet 2tam biet 2tam biet 2',
  },
  {
    id: 3,
    english: 'good bye 3',
    vietnamese: 'tam biet 3',
  },
  {
    id: 4,
    english: 'good bye 4',
    vietnamese: 'tam biet 4',
  },
  {
    id: 5,
    english: 'good bye 5',
    vietnamese: 'tam biet 5',
  },
  {
    id: 6,
    english: 'good bye 6',
    vietnamese: 'tam biet 6',
  },
  {
    id: 7,
    english: 'good bye 7',
    vietnamese: 'tam biet 7',
  },
  {
    id: 8,
    english: 'good bye 8',
    vietnamese: 'tam biet 8',
  },
  {
    id: 9,
    english: 'good bye 9',
    vietnamese: 'tam biet 9',
  },
];
