import React from 'react';

import {NavigationContainer, RouteProp} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from '../screens/home';
import ListCards from '../screens/listCards';
// import {ICard} from '../types/ICards';
import Study from '../screens/study';
import {
  EStatusAuth,
  getAuthAsync,
  IAuth,
  onLogin,
  updateStatusAuth,
} from '../reduxs/authSlice';
import {RootState} from '../reduxs/store';
import {useDispatch, useSelector} from 'react-redux';
import URL from '../config/Api';
import {ActivityIndicator, Alert} from 'react-native';
import {Colors, View} from 'react-native-ui-lib';
import Login from '../screens/auth/login';
import {ICard} from '../types/ICards';

export type RootStackParamList = {
  Login: undefined;
  Home: undefined;
  ListCards: {
    listId: number;
  };
  Study: {
    index: number;
  };
};

const Stack = createNativeStackNavigator();
const RootStack = () => {
  const statusAuth = useSelector<RootState, EStatusAuth>(
    state => state.auth.statusAuth,
  );
  const dispatch = useDispatch();
  const checkLogin = React.useCallback(async () => {
    const auth: IAuth | null = await getAuthAsync();
    if (auth) {
      //call api validate auth
      fetch(URL.ValidateToken, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          token: auth.token,
        }),
      })
        .then(response => response.json())
        .then((json: {success: boolean; message: string}) => {
          const success = json.success;
          //token fail
          if (!success) {
            Alert.alert('Thông báo', json.message);
            dispatch(updateStatusAuth({statusAuth: EStatusAuth.unauth}));
            return;
          }
          //token success
          dispatch(onLogin(auth));
          return json;
        });
    } else {
      dispatch(updateStatusAuth({statusAuth: EStatusAuth.unauth}));
    }
  }, []);
  React.useEffect(() => {
    checkLogin();
  }, []);

  if (statusAuth === EStatusAuth.check) {
    return (
      <View flex center>
        <ActivityIndicator color={Colors.primary} />
      </View>
    );
  }

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        {statusAuth === EStatusAuth.unauth ? (
          <>
            <Stack.Screen name="Login" component={Login} />
          </>
        ) : (
          <>
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen
              name="ListCards"
              component={ListCards}
              options={({route}) => ({
                title: 'List Card ' + route.params.listId.toString(),
              })}
            />
            <Stack.Screen name="Study" component={Study} />
          </>
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RootStack;
